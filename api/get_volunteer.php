<?php
include_once("db.php");
$response           = array();
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['phone']))
{
    $phone = $_POST['phone'];

    $get_user_data    = $conn->prepare("SELECT * FROM `block_poc` WHERE `phone`=:phone");
    $get_user_data->execute(array(':phone' => $phone));
    $Data             = $get_user_data->fetch(PDO::FETCH_ASSOC);


    if(!empty($Data))
    {
        $response['status']             = 1;
        $response['message']            = 'Data fetch successfully';
        $response['data']               = $Data;

        echo json_encode($response);
           
    }
    else
    {
        $response['status']             = 0;
        $response['message']            = "Volunteer doesn't exists";
        $response['data']               = NULL;

        echo json_encode($response);
    }
    
}
else
{

    $response['status']             = 0;
    $response['message']            = 'Request method not allowed';
    $response['data']               = NULL;

    echo json_encode($response);
}



