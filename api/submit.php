<?php
include_once("db.php");


$response           = array();


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $phone = $_POST['phone'];

    $get_user_data           = $conn->prepare("SELECT * FROM `block_poc` WHERE `phone`=:phone");
    $get_user_data->execute(array(':phone' => $phone));
    $Data             = $get_user_data->fetch(PDO::FETCH_ASSOC);
    // $Data             = array();


    if(!empty($Data))
    {
        $confirm_name = $_POST['confirm_name'];
        $modify_name = $_POST['name'];
        // $phone = isset($_POST['phone']) ? $_POST['phone'] : "";
        $district = isset($_POST['district']) ? $_POST['district'] : "";
        $AC = isset($_POST['AC']) ? $_POST['AC'] : "";
        $block = isset($_POST['block']) ? $_POST['block'] : "";
        $GPinfoArray = isset($_POST['GPinfoArray']) ? $_POST['GPinfoArray'] : "";
        $remarks = isset($_POST['remarks']) ? $_POST['remarks'] : "";
        
        
    
        $date = date('Y-m-d H:i:s');
        
        $updateCard = "UPDATE `block_poc` SET 
        `confirm_name`  = :confirm_name,
        `modify_name`   = :name,
        `district`      = :district,        
        `AC`            = :AC,
        `block`         = :block,
        `GPinfoArray`   = :GPinfoArray,        
        `submit_date`   = :submit_date,
        `remarks`       = :remarks  WHERE  
        `phone`         = :phone";
    
        $bindCard = $conn->prepare($updateCard);
        $bindCard->bindParam(':confirm_name', $confirm_name);
        $bindCard->bindParam(':name', $modify_name);
        $bindCard->bindParam(':district', $district);
        $bindCard->bindParam(':AC', $AC);
        $bindCard->bindParam(':block', $block);
        $bindCard->bindParam(':GPinfoArray', $GPinfoArray);
        $bindCard->bindParam(':phone', $phone);
        $bindCard->bindParam(':submit_date', $date);
        $bindCard->bindParam(':remarks', $remarks);       
        $bindCard->execute();
        $response['status']             = 1;
        $response['message']            = 'POC details submit successfully';
        $response['data']               = array();

        echo json_encode($response);
           
    }
    else
    {
        $response['status']             = 0;
        $response['message']            = "POC number doesn't exists";
        $response['data']               = NULL;

        echo json_encode($response);
    }
}



