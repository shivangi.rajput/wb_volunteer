<?php
    include_once("db.php");
    $stmt           = $conn->prepare("SELECT DISTINCT `org_district`  FROM `ac_block_gp_mapping` ORDER BY `org_district` ASC");
    $stmt->execute();     
    $result         = array();
    while($row      = $stmt->fetch()){
        $result[]   = $row['org_district'];
    }
    if(!empty($result))
    {
        $response['status']             = 1;
        $response['message']            = 'Data fetch successfully';
        $response['data']               = $result;

        echo json_encode($response);
           
    }
    else
    {
        $response['status']             = 0;
        $response['message']            = "Data doesn't exists";
        $response['data']               = NULL;

        echo json_encode($response);
    }
   

?>