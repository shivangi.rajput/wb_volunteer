<?php
include_once "db.php";
if (isset($_POST['district']) && isset($_POST['block']) && isset($_POST['ac'])) {
    $district = $_POST['district'];
    $block = $_POST['block'];
    $ac = $_POST['ac'];
    $stmt = $conn->prepare("SELECT DISTINCT `gp` FROM `ac_block_gp_mapping` WHERE `org_district`=:district AND `block`=:block AND `ac`=:ac ORDER BY `gp` ASC");
    $stmt->execute(array(':district' => $district, ':block' => $block, ':ac' => $ac));
    $result = array();
        // echo ($stmt); exit;
    while ($row = $stmt->fetch()) {
        $result[] = $row['gp'];
    }
    if(!empty($result))
    {
        $response['status']             = 1;
        $response['message']            = 'Data fetch successfully';
        $response['data']               = $result;

        echo json_encode($response);
           
    }
    else
    {
        $response['status']             = 0;
        $response['message']            = "Data doesn't exists";
        $response['data']               = NULL;

        echo json_encode($response);
    }

}
else
{

    $response['status']             = 0;
    $response['message']            = 'Request method not allowed';
    $response['data']               = NULL;

    echo json_encode($response);
}
?>
