<?php
include_once("db.php");
if (isset($_POST['district']) && isset($_POST['ac'])) {
    $district       = $_POST['district'];
    $ac             = $_POST['ac'];
    $stmt           = $conn->prepare("SELECT DISTINCT `block` FROM `ac_block_gp_mapping` WHERE `org_district`=:district AND `ac`=:ac ORDER BY `block` ASC");
    $stmt->execute(array(':district' => $district, ':ac' => $ac));
    $result         = array();
    while ($row      = $stmt->fetch()) {
        $result[]   = $row['block'];
    }
   
    if(!empty($result))
    {
        $response['status']             = 1;
        $response['message']            = 'Data fetch successfully';
        $response['data']               = $result;

        echo json_encode($response);
           
    }
    else
    {
        $response['status']             = 0;
        $response['message']            = "Data doesn't exists";
        $response['data']               = NULL;

        echo json_encode($response);
    }
}
else
{

    $response['status']             = 0;
    $response['message']            = 'Request method not allowed';
    $response['data']               = NULL;

    echo json_encode($response);
}
?>
